-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Sam 31 Mars 2018 à 11:00
-- Version du serveur :  5.7.14
-- Version de PHP :  7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `atlantik`
--

-- --------------------------------------------------------

--
-- Structure de la table `bateau`
--

CREATE TABLE `bateau` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `longueur` float NOT NULL,
  `largeur` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `bateau`
--

INSERT INTO `bateau` (`id`, `nom`, `longueur`, `largeur`) VALUES
(1, 'Luce isle', 37.2, 8.6),
(2, 'Al\' xi', 25, 7),
(3, 'Bateau Fret Bidon', 30, 8);

-- --------------------------------------------------------

--
-- Structure de la table `bateau_equipement`
--

CREATE TABLE `bateau_equipement` (
  `id_bateau` int(11) NOT NULL,
  `id_equipement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `bateau_equipement`
--

INSERT INTO `bateau_equipement` (`id_bateau`, `id_equipement`) VALUES
(1, 1),
(2, 1),
(1, 2),
(1, 3),
(2, 3),
(1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `bateau_fret`
--

CREATE TABLE `bateau_fret` (
  `id_bateau` int(11) NOT NULL,
  `poidsMax` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `bateau_fret`
--

INSERT INTO `bateau_fret` (`id_bateau`, `poidsMax`) VALUES
(3, 30000);

-- --------------------------------------------------------

--
-- Structure de la table `bateau_voyageur`
--

CREATE TABLE `bateau_voyageur` (
  `id_bateau` int(11) NOT NULL,
  `vitesse` float NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `bateau_voyageur`
--

INSERT INTO `bateau_voyageur` (`id_bateau`, `vitesse`, `image`) VALUES
(1, 26, 'luce_isle.jpg'),
(2, 16, 'al_xi.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `equipement`
--

CREATE TABLE `equipement` (
  `id` int(11) NOT NULL,
  `libelle` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `equipement`
--

INSERT INTO `equipement` (`id`, `libelle`) VALUES
(1, 'Accès Handicapé'),
(2, 'Bar'),
(3, 'Pont Promenade'),
(4, 'Salon Vidéo'),
(5, 'Restaurant'),
(6, 'Salle de Théâtre'),
(7, 'Salle de Cinéma'),
(8, 'Bowling'),
(9, 'Piscine'),
(10, 'Sauna / Hamam'),
(11, 'Salon de Massage');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `bateau`
--
ALTER TABLE `bateau`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bateau_equipement`
--
ALTER TABLE `bateau_equipement`
  ADD PRIMARY KEY (`id_bateau`,`id_equipement`),
  ADD KEY `id_equipement` (`id_equipement`);

--
-- Index pour la table `bateau_fret`
--
ALTER TABLE `bateau_fret`
  ADD PRIMARY KEY (`id_bateau`);

--
-- Index pour la table `bateau_voyageur`
--
ALTER TABLE `bateau_voyageur`
  ADD PRIMARY KEY (`id_bateau`);

--
-- Index pour la table `equipement`
--
ALTER TABLE `equipement`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `bateau`
--
ALTER TABLE `bateau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `equipement`
--
ALTER TABLE `equipement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `bateau_equipement`
--
ALTER TABLE `bateau_equipement`
  ADD CONSTRAINT `bateau_equipement_ibfk_1` FOREIGN KEY (`id_bateau`) REFERENCES `bateau_voyageur` (`id_bateau`),
  ADD CONSTRAINT `bateau_equipement_ibfk_2` FOREIGN KEY (`id_equipement`) REFERENCES `equipement` (`id`);

--
-- Contraintes pour la table `bateau_fret`
--
ALTER TABLE `bateau_fret`
  ADD CONSTRAINT `bateau_fret_ibfk_1` FOREIGN KEY (`id_bateau`) REFERENCES `bateau` (`id`);

--
-- Contraintes pour la table `bateau_voyageur`
--
ALTER TABLE `bateau_voyageur`
  ADD CONSTRAINT `bateau_voyageur_ibfk_1` FOREIGN KEY (`id_bateau`) REFERENCES `bateau` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
