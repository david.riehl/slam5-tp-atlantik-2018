﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlantik.Classes;
using Atlantik.Tools;
using MySql.Data.MySqlClient;

namespace Atlantik.Dao
{
    class BateauFretDao : AbstractDao<BateauFret>
    {
        public BateauFretDao() : base() { }

        public BateauFretDao(string path) : base(path) { }

        public BateauFretDao(DatabaseConfiguration dbConfig) : base(dbConfig) { }

        public int CreateTable()
        {
            Console.WriteLine("CREATE TABLE `bateau_fret` (...);");

            int res;

            string cmdText = @"
                CREATE TABLE IF NOT EXISTS `bateau_fret`
                (
                    `id_bateau` INT NOT NULL,
                    `poidsMax`  INT NOT NULL,
                    PRIMARY KEY (`id_bateau`),
                    FOREIGN KEY (`id_bateau`) REFERENCES `bateau`(`id`)
                )Engine=InnoDB;
            ";

            Open();
            MySqlCommand cmd = new MySqlCommand(cmdText, Connection);
            res = cmd.ExecuteNonQuery();
            Close();

            return res;
        }
        public override int Create(BateauFret item)
        {
            throw new NotImplementedException();
        }

        public override int Delete(BateauFret item)
        {
            throw new NotImplementedException();
        }

        public override BateauFret Read(int id)
        {
            throw new NotImplementedException();
        }

        public override List<BateauFret> ReadAll()
        {
            throw new NotImplementedException();
        }

        public override int Update(BateauFret item)
        {
            throw new NotImplementedException();
        }
    }
}
