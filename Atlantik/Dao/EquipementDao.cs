﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlantik.Classes;
using Atlantik.Tools;
using MySql.Data.MySqlClient;

namespace Atlantik.Dao
{
    class EquipementDao : AbstractDao<Equipement>
    {
        public EquipementDao() : base() { }

        public EquipementDao(string path) : base(path) { }

        public EquipementDao(DatabaseConfiguration dbConfig) : base(dbConfig) { }

        public int CreateTable()
        {
            Console.WriteLine("CREATE TABLE `equipement` (...);");

            int res;

            string cmdText = @"
                CREATE TABLE IF NOT EXISTS `equipement`
                (
                    `id` INT NOT NULL AUTO_INCREMENT,
                    `libelle` VARCHAR(50) NOT NULL,
                    PRIMARY KEY (`id`)
                )Engine=InnoDB;
            ";

            Open();
            MySqlCommand cmd = new MySqlCommand(cmdText, Connection);
            res = cmd.ExecuteNonQuery();
            Close();

            return res;
        }

        public int CreateLinkTable()
        {
            Console.WriteLine("CREATE TABLE `bateau_equipement` (...);");

            int res;

            string cmdText = @"
                CREATE TABLE IF NOT EXISTS `bateau_equipement`
                (
                    `id_bateau`     INT NOT NULL,
                    `id_equipement` INT NOT NULL,
                    PRIMARY KEY (`id_bateau`, `id_equipement`),
                    FOREIGN KEY (`id_bateau`) REFERENCES `bateau_voyageur`(`id_bateau`),
                    FOREIGN KEY (`id_equipement`) REFERENCES `equipement`(`id`)
                )Engine=InnoDB;
            ";

            Open();
            MySqlCommand cmd = new MySqlCommand(cmdText, Connection);
            res = cmd.ExecuteNonQuery();
            Close();

            return res;
        }

        public override int Create(Equipement item)
        {
            throw new NotImplementedException();
        }

        public override int Delete(Equipement item)
        {
            throw new NotImplementedException();
        }

        public override Equipement Read(int id)
        {
            throw new NotImplementedException();
        }

        public override List<Equipement> ReadAll()
        {
            string cmdText = @"
                SELECT *
                FROM `equipement`;
            ";

            Open();

            MySqlCommand cmd = new MySqlCommand(cmdText, Connection);
            MySqlDataReader reader = cmd.ExecuteReader();

            List<Equipement> equipements = new List<Equipement>();

            while (reader.Read())
            {
                int _id = reader.GetInt32("id");
                string _libelle = reader.GetString("libelle");

                Equipement equipement = new Equipement(_id, _libelle);

                equipements.Add(equipement);
            }

            reader.Close();
            Close();

            return equipements;
        }

        public List<Equipement> ReadAllByIdBateau(int id)
        {
            string cmdText = @"
                SELECT *
                FROM `equipement`
                INNER JOIN `bateau_equipement` ON `id_equipement` = `equipement`.`id`
                WHERE `id_bateau` = ?id;
            ";

            Open();

            MySqlCommand cmd = new MySqlCommand(cmdText, Connection);
            cmd.Prepare();
            cmd.Parameters.Add(new MySqlParameter("id", id));
            MySqlDataReader reader = cmd.ExecuteReader();

            List<Equipement> equipements = new List<Equipement>();

            while (reader.Read())
            {
                int _id = reader.GetInt32("id");
                string _libelle = reader.GetString("libelle");

                Equipement equipement = new Equipement(_id, _libelle);

                equipements.Add(equipement);
            }

            reader.Close();
            Close();

            return equipements;
        }

        public override int Update(Equipement item)
        {
            throw new NotImplementedException();
        }
    }
}
