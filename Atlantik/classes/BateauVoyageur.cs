﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlantik.Classes
{
    class BateauVoyageur : Bateau
    {
        // Propriété(s)

        public decimal Vitesse { get; set; }
        public string Image { get; set; }
        public List<Equipement> LesEquipements { get; set; }

        // Constructeur(s)

        public BateauVoyageur() { }

        public BateauVoyageur(int id, string nom, decimal longueur, decimal largeur)
            : base(id, nom, longueur, largeur) { }

        public BateauVoyageur(int id, string nom, decimal longueur, decimal largeur, 
            decimal vitesse, string image, List<Equipement> lesEquipements)
            : base(id, nom, longueur, largeur)
        {
            Vitesse = vitesse;
            Image = image;
            LesEquipements = lesEquipements;
        }

        // Méthode(s)

        /// <summary>
        /// Retourne sous la forme d'une chaîne toutes les valeurs concaténées des attributs de la
        /// classe, sauf l'attribut imageBatVoy qui n'est pas inséré dans la chaîne concaténée.
        /// Chaque valeur est précédée de son libellé.
        /// Exemple : Nom du bateau : Luce isle
        ///           Longueur : 37,20 mètres
        ///           Largeur  : 8,60 mètres
        ///           Vitesse  : 26 noeuds
        ///           Liste des équipements du bateau :
        ///            - Accès Handicapé
        ///            - Bar 
        ///            - Pont Promenade
        ///            - Salon Vidéo
        /// Remarque :
        /// On utilisera l’opérateur "+" pour concaténer des valeurs de type string et 
        /// la propriété de classe Environment.NewLine pour marquer une fin de ligne.
        /// Exemple : str = "Liste des équipements du bateau : " + Environment.NewLine;
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string str;

            str = base.ToString();
            str += "Vitesse : " + Vitesse + " noeuds" + Environment.NewLine;
            str += "Liste des équipements du bateau : " + Environment.NewLine;

            foreach (Equipement equipement in LesEquipements)
            {
                str += " - " + equipement.ToString() + Environment.NewLine;
            }

            return str;
        }
    }
}
